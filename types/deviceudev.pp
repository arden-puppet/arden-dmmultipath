type Dmmultipath::DeviceUdev = Hash[
  Enum[
    'id_vendor',
    'id_model',
    'timeout',
  ],
  Variant[
    String,
    Integer,
  ],
]
