type Dmmultipath::Failback = Variant[
  Integer,
  Enum[
    'immediate',
    'manual',
    'followover',
  ],
]
