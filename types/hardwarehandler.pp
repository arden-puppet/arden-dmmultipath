type Dmmultipath::HardwareHandler = Enum[
  '0',
  '1 emc',
  '1 rdac',
  '1 hp_sw',
  '1 alua',
]
