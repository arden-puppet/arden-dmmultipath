
# dmmultipath

[![pipeline status](https://gitlab.com/arden-puppet/arden-dmmultipath/badges/master/pipeline.svg)](https://gitlab.com/arden-puppet/arden-dmmultipath/commits/master) [![Version](https://img.shields.io/puppetforge/v/arden/dmmultipath.svg)](https://forge.puppet.com/arden/dmmultipath)

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with dmmultipath](#setup)
    * [Beginning with dmmultipath](#beginning-with-dmmultipath)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)
    * [Changelog](#changelog)
    * [Contributors](#contributors)

## Description

This module configures the DM Multipath module for a given device or set of devices.

## Setup

There are no external dependencies for this module aside from standard system packages.

### Beginning with dmmultipath

For this module to work the class must be included with at least one enabled storage controller.

```puppet
class { 'dmmultipath':
  enabled_devices => ['ibm_2145'],
}
```

This can also be achieved via hiera.

```yaml
---
dmmultipath::enabled_devices:
  - 'ibm_2145'
```

```puppet
include 'dmmultipath'
```

## Usage

The base configuration will provide sane defaults for the ibm_2145 device, enable the multipathd service at boot, and configure a 120 second timeout for all corresponding disk devices via udev. In addition, a regeneration of the initramfs for the current kernel can also be configured. This ensures that the appropriate multipath settings are available from the time of boot.

```puppet
class { 'dmmultipath':
  regen_initramfs => true,
  enabled_devices => ['ibm_2145'],
}
```

## Limitations

### Operating System Support

* RedHat Family 7.x (CentOS and RHEL)

### Controller Support

A full listing of supported types can be found in the definition of the [Dmmultipath::SupportedModels](./types/supportedmodels.pp) type.

* IBM Spectrum Virtualize (SVC and Storwize) systems.

## Development

TBD. Create an issue and we'll figure it out from there.

### Contributors

Check out the [contributor list](https://gitlab.com/arden-puppet/arden-dmmultipath/graphs/master).

### Changelog

All feature updates for this module can be found in the [changelog](./CHANGELOG.md).
