# Private class responsible for creating config files & directory structure
#
# @summary Create the directory structure and each config file
class dmmultipath::configure {
  # Create the directory structure
  $delimiter = '/'
  $dir_parts = split($dmmultipath::config_dir, $delimiter)
  $dir_parts.each | $index, $path_part | {
    # Skip the bottom index as it's just `//`
    if $index < 1 {
      $create_level = false
    } elsif $index == 1 {
      # Let's avoid managing some of the base directory structure
      case $path_part {
        'etc': { $create_level = false }
        default: { $create_level = true }
      }
    } else {
      $create_level = true
    }

    # Determine the path to this level
    if $index < (length($dir_parts) + 1) {
      $dir_parts_current = $dir_parts[0, $index + 1]
      $dir_path = join($dir_parts_current, $delimiter)
    } else {
      $dir_path = $dmmultipath::config_dir
    }

    # Create the directory
    if $create_level {
      file { $dir_path:
        ensure => 'directory',
        owner  => 'root',
        group  => 'root',
        mode   => '0755',
      }
    }
  }

  # Create the main multipath.conf
  file { '/etc/multipath.conf':
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0664',
    content => epp('dmmultipath/multipath.conf.epp'),
  }

  # Iterate through the provided device listing and create config files
  $dmmultipath::enabled_devices.each | $device_type | {
    $device_parameters = $dmmultipath::dev_data[$device_type]
    $device_file = "${dmmultipath::config_dir}/${device_type}.conf"
    file { $device_file:
      ensure  => 'file',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => epp('dmmultipath/device.conf.epp', $device_parameters),
    }

    # Add a udev rule if the device has rule data to configure
    if $dmmultipath::udev_rules and $device_type in $dmmultipath::dev_udev_data {
      $device_udev_parameters = $dmmultipath::dev_udev_data[$device_type]
      $vendor = downcase($device_udev_parameters['id_vendor'])
      $product = downcase($device_udev_parameters['id_model'])
      $udev_rule_file =
        "${dmmultipath::udev_rules_dir}/99-${vendor}-${product}.rules"

      file { $udev_rule_file:
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => epp(
          'dmmultipath/udev.rules.epp',
          $dmmultipath::dev_udev_data[$device_type]
        ),
      }
    }
  }
}
