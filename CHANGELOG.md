# Changelog

All notable changes to this project will be documented in this file.

## Release 0.1.2

[Full Changelog](https://gitlab.com/arden-puppet/arden-dmmultipath/compare/0.1.1...0.1.2#)

**Features**
* Updated PDK to version 1.13.x

**Bugfixes**
* Converted the initramfs generation code such that it operates via refreshonly instead of attempting to checksum the resulting file

## Release 0.1.1

[Full Changelog](https://gitlab.com/arden-puppet/arden-dmmultipath/compare/0.1.0...0.1.1#)

**Features**
* Updated PDK to version 1.9.x

**Bugfixes**
* Switched package declaration ensure statement from 'installed' to 'present' for compatibility with the stdlib function ensure_packages

## Release 0.1.0

**Features**
* Support for IBM Storwize / SVC

**Bugfixes**
* None yet...

**Known Issues**
* None known...
